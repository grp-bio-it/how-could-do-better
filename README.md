# How could do better

This repository contains presentations and examples of
*how we could do better* when providing scientific feedback.
It complements the discussions in the
[how could do better](https://chat.embl.org/embl/channels/how-could-be-done-better)
channel in the EMBL Chat.

## How it started

It is in the nature of a scientist to be inquisitive, critical and sometimes sceptical about everything science.
With a positive mindset, these traits are essential to develop new ideas and contribute to the advancement of science.
When evaluating the work of others, however, we often fall victim of excessive criticism, identifying the many wrongs but not taking the extra step of discussing how to make it better.
Also known as "bashing" and "trash talking", this behavior rarely results in progress or generation of new ideas.

This initiative was created to nurture a culture of positivity towards science, uplifting ideas and encouraging constructive feedback.

## License

All work in this repository is licensed under the MIT license unless otherwise specified.
Notable exceptions include images and other referenced online materials.

